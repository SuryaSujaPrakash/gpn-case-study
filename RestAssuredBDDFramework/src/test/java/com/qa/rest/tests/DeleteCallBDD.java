package com.qa.rest.tests;

import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class DeleteCallBDD {
	
	
	@Test
	public void deleteData() {
		RestAssured.baseURI="https://reqres.in";
		RestAssured.basePath="/api/users/2";
		
	   given()
		
		.when()
		  .delete()
		.then()
		  .statusCode(204)
		  .log().all();
	     

		  
		
	}

}
