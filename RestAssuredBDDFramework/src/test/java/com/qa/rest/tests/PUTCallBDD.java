package com.qa.rest.tests;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import io.restassured.RestAssured;

public class PUTCallBDD {
	
	public static HashMap map=new HashMap();
	
	//String email=RestUtils.getemail();
	//String last_name=RestUtils.getlast_Name();
	//int id=2;
	
	@BeforeClass
	public void putData() {
		map.put("email",RestUtils.getemail());
		map.put("last_name",RestUtils.getlast_Name());
		
		RestAssured.baseURI="https://reqres.in";
		RestAssured.basePath="/api/users/3";
	}
	
	@Test
	public void testPUT() {
		given()
		 .contentType("application/json")
		 .body(map)
		.when()
		 .put()
		.then()
		 .statusCode(200)
		 .log().all();
	}
}
