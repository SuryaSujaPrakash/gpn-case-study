package com.qa.rest.tests;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class GetCallBDD {
	@Test
	public void getUserDetails() {
		given()
		.when()
		 .get("https://reqres.in/api/users/2")
		.then()
		  .statusCode(200)
		  .assertThat().body("data.email",equalTo("janet.weaver@reqres.in"))
		  .assertThat().body("data.first_name",equalTo("Janet"))
		  .assertThat().body("data.last_name",equalTo("Weaver"))
		  .assertThat().body("data.first_name",equalTo("Janet"))
		  .headers("Content-Type", "application/json; charset=utf-8")
		  .log().all();

	}

}
