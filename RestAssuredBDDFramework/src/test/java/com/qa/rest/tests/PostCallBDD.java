package com.qa.rest.tests;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import io.restassured.RestAssured;

public class PostCallBDD {
	public static HashMap map=new HashMap();
	
	@BeforeClass
	public void postdata() {
		
		map.put("first_name",RestUtils.getFirst_Name());
		map.put("last_name",RestUtils.getLast_Name());
		map.put("job",RestUtils.getJob());
		
		RestAssured.baseURI="https://reqres.in";
		RestAssured.basePath="/api/users";
	}
	
	@Test
	public void testPost() {
		given()
		  .contentType("application/json")
		  .body(map)
		
	    .when()
	      .post()
		
		.then() 
		  .statusCode(201)
		  .and()
		  .body("first_name",equalTo("Surya"))
		  .body("last_name",equalTo("S Prakash"))
		  .body("job",equalTo("engineer"))
		  .headers("Content-Type", "application/json; charset=utf-8")
		  .log().all();
		  
		 
		
	}
	
}
