package com.amazon.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amazon.base.TestBase;

public class SearchPage extends TestBase{
	
	  @FindBy(xpath = "//div[@class='s-widget-container s-spacing-small s-widget-container-height-small celwidget slot=MAIN template=SEARCH_RESULTS widgetId=search-results_1']//h2[@class='a-size-mini a-spacing-none a-color-base s-line-clamp-2']//span[1]")
	    WebElement lenovo;
	  @FindBy(id="productTitle")
	    WebElement Product;
	  @FindBy(linkText="Lenovo IdeaPad Slim 3 10th Gen Intel Core i3 15.6\" FHD Thin & Light Laptop(8GB/1TB HDD/2Yr Warranty/Windows 11/Office 2021/220Nits/3months Xbox Game Pass/Platinum Grey/1.7Kg),81WB018YIN")
	  WebElement cartName;
	  
	  @FindBy(xpath="//span[@class='a-truncate-cut'][contains(text(),'Lenovo IdeaPad Slim 3 10th Gen Intel Core i3 15.6\"')]")
	  WebElement cartName1;
    
	  @FindBy(id="add-to-cart-button")
	  WebElement cart;
	  
	  @FindBy(xpath="//span[@id='nav-cart-count']")
	  WebElement cartCount;
	  
	  @FindBy(xpath="//span[@class='nav-cart-icon nav-sprite']")
	  WebElement checkCartCount;
	  
	  @FindBy(xpath="//input[@value=\"Delete\"]")
	  WebElement delete;
	  
	  @FindBy(id="quantity_5")
	  WebElement quantity;
	  
	  @FindBy(xpath="//span[@class='a-dropdown-label']")
	  WebElement checkQuantity;
	  
	  @FindBy(id="a-autoid-4-announce")
	  WebElement qtyCount;
	  
	public SearchPage() {
		PageFactory.initElements(driver, this);
	}
	 public boolean ValidateProduct(){
	        lenovo.click();
	        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	          driver.switchTo().window(tabs.get(1));
	        return Product.isDisplayed();
	    }
	public String addToCart() {
		 lenovo.click();
	        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	          driver.switchTo().window(tabs.get(1));
	          cart.click();
	          driver.switchTo().window(tabs.get(0)).navigate().refresh();

	         cartCount.click();
	         String productName= cartName1.getText();
	         return productName;
	}
	public String cartDelete() {
		lenovo.click();
        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
          driver.switchTo().window(tabs.get(1));
          cart.click();
          driver.switchTo().window(tabs.get(0)).navigate().refresh();
          cartCount.click();
          delete.click();  
          String s=checkCartCount.getText();
          return s;
         
	}
	public String cartQuantity() {
		lenovo.click();
        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
          driver.switchTo().window(tabs.get(1));
          cart.click();
          driver.switchTo().window(tabs.get(0)).navigate().refresh();
          cartCount.click();
          checkQuantity.click();  
          quantity.click();
          String s=qtyCount.getText();
          System.out.println(s);
          return s;
         
}
}
	
	
	
	
	
	



