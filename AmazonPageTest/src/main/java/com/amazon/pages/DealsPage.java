package com.amazon.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amazon.base.TestBase;

public class DealsPage extends TestBase {
	@FindBy(xpath="//span[@class='nav-a-content'][normalize-space()='All Deals']")
	WebElement allDeals;
	
	@FindBy(xpath="//div[@id='nav-progressive-subnav']//a[5]")
	WebElement coupons;
	
	@FindBy(xpath="//span[normalize-space()='Watched Deals']")
	WebElement watchdeal;
	
    @FindBy(xpath="//span[normalize-space()='Amazon Assistant']")
    WebElement amazonAssistant;

	public DealsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public boolean verifyallDeals() {
		return allDeals.isDisplayed();
	}
	
	 public boolean ValidateCoupon(){
	        return coupons.isDisplayed();
	 }
	 
	 public boolean ValidateWatchDeal(){
	        return watchdeal.isDisplayed();
	 }
	 
	 public boolean ValidateamazonAssistant() {
		 return amazonAssistant.isDisplayed();
	 }
}
