package com.amazon.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import com.amazon.base.TestBase;


public class HomePage extends TestBase {
	
	@FindBy(xpath="//a[@id='nav-logo-sprites']")
	WebElement logo;
	
    @FindBy(xpath="//input[@id='twotabsearchtextbox']")
    WebElement search;
    
    @FindBy(xpath="//input[@id='nav-search-submit-button']")
    WebElement submitBtn;
    
    @FindBy(linkText="Today's Deals")
    WebElement deal;
    
    @FindBy(xpath="//span[@class='nav-cart-icon nav-sprite']")
    WebElement cart;
    
    @FindBy(xpath = "//i[@class='a-icon a-icon-next-rounded']")
    WebElement cauroselCheck;

   
	public HomePage( ) {
		PageFactory.initElements(driver, this);
	}
	
	public boolean verifyLogo() {
		return logo.isDisplayed();
	}
	
    public SearchPage ValidateSearch(){
        search.sendKeys("lenovo");
        submitBtn.click();
        return new SearchPage();
		
	}
    public DealsPage clickOnDeal() {
    	deal.click();
    	return new DealsPage();
    }
    public CartPage clickOnCart() {
		cart.click();
		return new CartPage();
	}
    public boolean checkCaurosel(){
        boolean f=cauroselCheck.isDisplayed();
        return f;
    }
    
}
