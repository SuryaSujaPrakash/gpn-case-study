package com.amazon.testcases;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.amazon.base.TestBase;
import com.amazon.pages.CartPage;
import com.amazon.pages.DealsPage;
import com.amazon.pages.HomePage;
import com.amazon.pages.SearchPage;

public class HomePageTest extends TestBase{
	HomePage homePage;
	SearchPage searchPage;
	DealsPage dealsPage;
	CartPage cartPage;
	public HomePageTest() {
		super();
	}

	@BeforeMethod
    public void setUp() throws InterruptedException {
        initialization();
        homePage=new HomePage();
        searchPage=new SearchPage();
        dealsPage=new DealsPage();
        cartPage=new CartPage();
	}
	
	@Test(priority=1)
	public void verifyLogo() {
	   Assert.assertTrue(homePage.verifyLogo());
		
	}
	
	@Test(priority=2)
	public void verifysearch() {
		searchPage=homePage.ValidateSearch();
	}
	
	@Test(priority=3)
	public void clickOnDeal() {
		dealsPage=homePage.clickOnDeal();
	}
	
	@Test(priority=4)
	public void clickOnCart() {
		cartPage=homePage.clickOnCart();
	}
	
	@Test(priority=5)
	public void checkCauroselTest(){
        boolean name= homePage.checkCaurosel();
        Assert.assertTrue(name);
    }
	
	
	
	
	
	
	

   	 
   	 @AfterMethod
   	    public void tearDown(){
   	        driver.quit();
   	  
	
}
}
