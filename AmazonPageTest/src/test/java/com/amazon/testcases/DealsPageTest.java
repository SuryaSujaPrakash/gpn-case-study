package com.amazon.testcases;

import org.junit.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.amazon.base.TestBase;
import com.amazon.pages.DealsPage;
import com.amazon.pages.HomePage;

public class DealsPageTest extends TestBase{
	HomePage homePage;
	DealsPage dealsPage;
 
	public DealsPageTest() {
		super();
	 
 }
 	@BeforeMethod
    public void setUp() throws InterruptedException {
        initialization();
        homePage=new HomePage();
        dealsPage=homePage.clickOnDeal();
}
 	@Test(priority=1)
 	public void verifyallDeals() {
 		Assert.assertTrue(dealsPage.verifyallDeals());
 	}
 	@Test(priority=2)
 	
 	public void ValidateCoupon(){
        boolean status=dealsPage.ValidateCoupon();
        Assert.assertTrue(status);
    }
 	
 	@Test(priority=3)
 	public void ValidateWatchdeal(){
 	    boolean status=dealsPage.ValidateWatchDeal();
 	        Assert.assertTrue(status);
 	    }
 	
 	@Test(priority=4)
 	public void ValidateamazonAssistant() {
 		boolean status=dealsPage.ValidateamazonAssistant();
 		Assert.assertTrue(status);
 	}
 	
 	
 	
 	
 	
 	

 	@AfterMethod
    public void tearDown(){
        driver.quit();
  
 	
 	
 	
 	
}
}
