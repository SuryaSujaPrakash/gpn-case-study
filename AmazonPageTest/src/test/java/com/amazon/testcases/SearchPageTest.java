package com.amazon.testcases;

import org.junit.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.amazon.base.TestBase;
import com.amazon.pages.CartPage;
import com.amazon.pages.HomePage;
import com.amazon.pages.SearchPage;

public class SearchPageTest extends TestBase{
	SearchPage searchPage;
	HomePage homePage;

    
	public SearchPageTest() {
    	super();
    	

    }
 

   @BeforeMethod
   public void setUp() throws InterruptedException {
       initialization();
       homePage=new HomePage();
       searchPage=homePage.ValidateSearch();
       
  }
  @Test(priority=1)
  public void validateProducts() {
	 boolean status=searchPage.ValidateProduct();
  Assert.assertTrue(status);
	   
 }
   @Test(priority=2)
   public void addToCart() {
   String Name=searchPage.addToCart();
	  Assert.assertTrue(Name.contains("Lenovo IdeaPad Slim 3 10th Gen Intel Core"));
	
 }
  @Test(priority=3)
 public void checkcartDeleteTest() {
	   String value=searchPage.cartDelete();
 Assert.assertEquals(value, "");
 }
   @Test(priority=4)
  
   public void checkcartQuantityTest() {
   String value=searchPage.cartQuantity();
   Assert.assertEquals(value, "Qty:5");
 }
   
   @AfterMethod
   public void tearDown() {
	   driver.quit();
   }
}